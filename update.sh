#!/bin/bash

# Push any changes in the SVN tracking repos
if [ -d "../svn" ]; then

   for repo in `ls ../svn`
   do
      pushd ../svn/${repo}
      git push cfmm cfmm
      popd
   done

fi

# Update the local bare clone repos that exist
for repo in `ls -d ../*.git`
do
   pushd ${repo}
   git fetch
   popd
done

# Pull the latest version of the submodule repos
workingdir=`pwd`
for repo in `ls -d dcm4chee*/`
do

   pushd ${repo}
   git pull
   popd

   # Create missing local versions of repositories
   if [ ! -d "../${repo%?}.git" ]; then
      pushd ..
      git clone --bare git@gitlab.com:cfmm/dcm4chee/${repo}.git
      popd
   else
      pushd ../${repo%?}.git
      git fetch ${workingdir}/${repo}
      popd
   fi

done

