Project for building dcm4chee-web package

Linking with SVN
----------------
dcm4chee-* submodules are clones from SVN repository maintained by dcm4che group 
made using `git svn`. However the SVN information is lost when the git repository
is re-cloned. You can however add this information back again by editing the
 `.git/config` file for the git repository and adding.

```
[svn-remote "svn"]
	url = https://svn.code.sf.net/p/dcm4che/svn
	fetch = dcm4chee/dcm4chee-arc3-entities/trunk:refs/remotes/dcm4che/trunk
	branches = dcm4chee/dcm4chee-arc3-entities/branches/*:refs/remotes/dcm4che/*
	tags = dcm4chee/dcm4chee-arc3-entities/tags/*:refs/remotes/dcm4che/tags/*
```

After this you must run the  `git svn fetch` command
 * First time will take a long time to run as all SVN information must be download
 * `git svn fetch` updates the master branch to track the trunk of the SVN repository
 * To get upstream changes into the cfmm branch you have to either
    * Merge master to cfmm
    * Tag cfmm branch and then rebase on master

`git svn` converts SVN tags to git branches. The following script will convert 
these branches into git tags

```
git for-each-ref --format="%(refname:short) %(objectname)" refs/remotes/dcm4che/tags \
| while read BRANCH REF
  do
        TAG_NAME=${BRANCH#*/}
        BODY="$(git log -1 --format=format:%B $REF)"
        echo "ref=$REF parent=$(git rev-parse $REF^) tagname=$TAG_NAME body=$BODY" >&2
        git tag -a -m "$BODY" $TAG_NAME $REF^  &&\
        git branch -r -d $BRANCH
  done
```

Docker
-----

To build and update docker image, run the following commands

```
docker build -t latest .
docker tag latest registry.gitlab.com/cfmm/dcm4chee/build-web/image
docker push registry.gitlab.com/cfmm/dcm4chee/build-web/image
```

Building
--------

Project can be built with Gitlab CI

```
docker login
gitlab-ci-multi-runner-darwin-amd64 exec docker --cache-dir /cache --docker-volumes /Users:/Users --docker-volumes `pwd`/build-output:/cache build
```

Gitlab CI Multi-runner is deprecated in favor of `gitlab-runner`
Unfortunately, `gitlab-runner exec` does not handle artifacts correctly, i.e. at all. A workaround to get
artifacts is to copy them to `/cache` so they are available after exit. This means editing `.gitlab-ci.yml` for 
local versus repo builds.

```
gitlab-runner exec docker --cache-dir /cache --docker-volumes /Users:/Users --docker-volumes `pwd`/build-output:/cache build
```

For Gitlab to handle submodules, they must be relative to the parent project. For local testing of CI, it is therefore necessary to create
versions of the submodule projects in the same relative location within the local filesystem

```
cd ../
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-arc3-entities.git
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-cleanup.git
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-dashboard.git
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-icons.git
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-usr.git
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-web-common.git
git clone --bare git@gitlab.com:cfmm/dcm4chee/dcm4chee-web.git
```

The `update.sh` script will also create these bar repositories as well as pull the latest `cfmm` branch for all submodules.

