FROM ubuntu:14.04.5
RUN apt update && apt -y install openjdk-6-jdk maven2 wget
RUN wget https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem.txt -O /tmp/letsencrypt.pem && \
    keytool -import -trustcacerts -keystore /usr/lib/jvm/java-6-openjdk-amd64/jre/lib/security/cacerts -storepass changeit -noprompt -alias mycert -file /tmp/letsencrypt.pem && \
    rm -rf /tmp/letsencrypt.pem
